class DummyRepository {

   getAll = () => {
        return [
            {
               "name":"Adrián mock 2",
               "lastName":"Prause"
            },
            {
               "name":"Lucas",
               "lastName":"Brunini"
            },
            {
               "name":"Joel",
               "lastName":"Marrufo"
            },
            {
               "name":"Gastón",
               "lastName":"Fleitas"
            }
         ];
    };
  
  }
  
  module.exports = DummyRepository;