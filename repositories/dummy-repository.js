class DummyRepository {

 getAll = () => {
      return [
          {
             "name":"Adrián",
             "lastName":"Prause"
          },
          {
             "name":"Lucas",
             "lastName":"Brunini"
          },
          {
             "name":"Joel",
             "lastName":"Marrufo"
          },
          {
             "name":"Gastón",
             "lastName":"Fleitas"
          }
       ];
  };

}

module.exports = DummyRepository;