const router = require("express").Router();
const { ensureAuthenticated, ensureAuthorized } = require("../middleware/auth-middleware");
const {validationRules, validate} = require("../validations/category-validator");
const TopicsController = require("../controllers/topics-controller");

router.get("/topics", async (req, res) => {  
    // #swagger.tags = ['Topics']
    topicsController = new TopicsController();
    await topicsController.getAll(req, res);
});

router.put("/topics", async (req, res) => { 
    /*  #swagger.tags = ['Topics']
        #swagger.security = [{
        "Authorization": []
        }]
    	#swagger.parameters['obj'] = {
            in: 'body',
            required: true,
            schema: {
            $id: 1,
            $follow: true,
        }
    } */  
    topicsController = new TopicsController();
    await topicsController.save(req, res);
});

module.exports = router;