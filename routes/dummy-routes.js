const router = require("express").Router();
const { ensureAuthenticated, ensureAuthorized } = require("../middleware/auth-middleware");
const {validationRules, validate} = require("../validations/category-validator");
const { getAllDummy } = require("../controllers/dummy-controller");

router.get("/dummies", async (req, res) => {  
    // #swagger.tags = ['dummy']
    await getAllDummy(req, res);
});

module.exports = router;