const router = require("express").Router();

const categoriesRoutes = require("./categories-routes");
const dummyRoutes = require("./dummy-routes");
const authorsRoutes = require("./authors-routes");
const topicsRoutes = require("./topics-routes")

router.use("/api", categoriesRoutes);
router.use("/api", dummyRoutes);
router.use("/api/settings", authorsRoutes);
router.use("/api", topicsRoutes)

module.exports = router;