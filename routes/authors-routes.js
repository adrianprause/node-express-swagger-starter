const router = require("express").Router();
const { ensureAuthenticated, ensureAuthorized } = require("../middleware/auth-middleware");
const {validationRules, validate} = require("../validations/category-validator");
const AuthorsController = require("../controllers/authors-controller");

router.get("/authors", async (req, res) => {  
    // #swagger.tags = ['Authors']
    authorsController = new AuthorsController();
    await authorsController.getAll(req, res);
});

router.put("/authors", async (req, res) => { 
    /*  #swagger.tags = ['Authors']
        #swagger.security = [{
        "Authorization": []
        }]
    	#swagger.parameters['obj'] = {
            in: 'body',
            required: true,
            schema: {
            $id: 1,
            $follow: true,
        }
    } */  
    authorsController = new AuthorsController();
    await authorsController.save(req, res);
});

module.exports = router;