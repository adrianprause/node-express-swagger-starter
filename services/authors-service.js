var {DiContainer} = require("bubble-di");

class AuthorsService {

   constructor() {
      this.repository = DiContainer.getContainer().resolve("authors-repository");
   }

   getAll = () => {
      return this.repository.getAll(this.repository.getAll());
   };

   save = (id, follow) => {
      if (!this.repository.save(id, follow)) {
         throw new Error('Cannot update the item')
      }
   }

}

module.exports = AuthorsService