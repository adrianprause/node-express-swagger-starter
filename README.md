# Base API using NodeJS (Express) with Swagger

In this repo, we develop the REST API with the following features:

1. Authentication and authorization using PassportJS and JWT
2. Model validations using express-validator
3. Data modeling and schema construction using Mongoose
4. Dummy model api
5. Swagger documentation with authentication and image uploading using swagger-autogen
6. Nodemon for hot reloading
7. Custom-env form multiple env support
8. MongoDB
9. Mongoose

---

## Requirements

For development, you will need Node.js, Docker installed in your development machine

## Getting Started

```sh
# clone it
https://bitbucket.org/ageabit/api-base-mobile/src/master/
cd api-base-mobile

# Create docker machine
# docker build . -t agea/api-base-mobile
# Run the docker machine usin docker: docker run -e NODE_ENV=mock agea/api-base-mobile

# Build docker machine usin docker compose: docker-compose build <ENV>
docker-compose build mock

# Run docker machine usin docker compose: docker-compose up <ENV>
docker-compose up mock

## Swagger documentation (UI)
http://localhost:4000/doc
