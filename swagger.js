const swaggerAutogen = require("swagger-autogen")()

const doc = {
    info: {
        version: "1.0.0",
        title: "Agea Mobile API",
        description: "API base para apps mobile."
    },
    host: "localhost:4000",
    basePath: "/",
    schemes: ["http", "https"],
    consumes: ["application/json"],
    produces: ["application/json"],
    tags: [
    ],
    securityDefinitions: {
        Authorization: {
            type: "apiKey",
            name: "Authorization",
            description: "Value: Bearer ",
            in: "header",
            scheme: 'bearer'
        }
    },
    definitions: {
        CategoryModel: {
            $title: "Comedy",
        },
        DummyModel: {
            $name: "Adrián",
            $lastName: "Prause",
        },
        AuthorModel: {
            $id: 1,
            $name: "Adrián Prause",
            $follow: true,
        }
    }
};

const outputFile = "./swagger_output.json";
const endpointFiles = ["./routes/index.js"];

swaggerAutogen(outputFile, endpointFiles, doc).then(() => {
    require("./index");
});