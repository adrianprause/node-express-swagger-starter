const Environment = require('custom-env')
const cors = require("cors");
const express = require("express");
const paginate = require("express-paginate");
const passport = require("passport");
const { connect } = require("mongoose");
const swaggerUi = require("swagger-ui-express");
const swaggerFile = require("./swagger_output.json");
const app = express();
const router = require("./routes/index");
var { DiContainer } = require("bubble-di");

// Environment setup
Environment.env();

// Dependency injections
DiContainer.setContainer(new DiContainer());
var dummyRepository;
var authorsRepository;
var topicsRepository;
if (process.env.APP_ENV == 'mock') {
    dummyRepository = require("./repositories/dummy-mock-repository");
    authorsRepository = require("./repositories/authors-mock-repository");
    topicsRepository = require("./repositories/topics-mock-repository")
} else {
    dummyRepository = require("./repositories/dummy-repository");
    authorsRepository = require("./repositories/authors-mock-repository");
    topicsRepository = require("./repositories/topics-mock-repository")
}
DiContainer.getContainer().registerInstance("dummy-repository", new dummyRepository());
DiContainer.getContainer().registerInstance("authors-repository", new authorsRepository());
DiContainer.getContainer().registerInstance("topics-repository", new topicsRepository());

// Init app
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(passport.initialize());
require("./middleware/passport-middleware")(passport);

app.use(paginate.middleware(process.env.LIMIT, process.env.MAX_LIMIT));
app.use(router);
app.use("/doc", swaggerUi.serve, swaggerUi.setup(swaggerFile));

const runApp = async () => {
    try {
        console.log(`Server started as: ${process.env.APP_ENV}`);
        if (process.env.APP_ENV == 'dev') {
            console.log(`Init connection to database ${process.env.MONGO_DB}`);
            await connect(process.env.MONGO_DB, {
                useFindAndModify: false,
                useUnifiedTopology: true,
                useNewUrlParser: true,
                useCreateIndex: true,
            });
            console.log(`Successfully connected to database ${process.env.MONGO_DB}`);
        }
        app.listen(process.env.PORT, () => {
            console.log(`Server started successfully on PORT ${process.env.PORT}`);
        })
    } catch(err) {
        console.log(err);
        runApp();
    }
};

runApp();