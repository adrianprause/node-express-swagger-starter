FROM node:latest

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install dependencies
COPY package.json .
RUN npm install

# Bundle app source
COPY index.js ./

ARG NODE_ENV=mock
ENV NODE_ENV=${NODE_ENV}

# Exports
EXPOSE 4000
CMD [ "npm", "run" ]