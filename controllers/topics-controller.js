const TopicsService = require("../services/topics-service");

class TopicsController {

    constructor() {
        this.topicsService = new TopicsService();
    }
        
    getAll = async(req, res) => {
        try {
            return res.status(201).json(this.topicsService.getAll());
        } catch(err) {
            return res.status(500).json({
                message: err.message,
                success: false,
            });
        }
    };
        
    save = async(req, res) => {
        try {
            this.topicsService.save(req.params.id, req.params.follow);
            return res.status(201).json({
                message: "Item successfully updated",
                success: true,
            });
        } catch(err) {
            console.log(err);
            return res.status(500).json({
                message: err.message,
                success: false,
            });
        }
    };

}

module.exports = TopicsController;