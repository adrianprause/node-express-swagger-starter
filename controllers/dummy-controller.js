
var {DiContainer} = require("bubble-di");

const getAllDummy = async(req, res) => {
    try {
        dummyRepository = DiContainer.getContainer().resolve("dummy-repository");
        return res.status(201).json(dummyRepository.getAll());
    } catch(err) {
        return res.status(500).json({
            message: err.message,
            success: false,
        });
    }
};

module.exports = {
    getAllDummy
}