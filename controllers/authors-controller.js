const AuthorsService = require("../services/authors-service");

class AuthorsController {

    constructor() {
        this.authorsService = new AuthorsService();
    }
        
    getAll = async(req, res) => {
        try {
            return res.status(201).json(this.authorsService.getAll());
        } catch(err) {
            return res.status(500).json({
                message: err.message,
                success: false,
            });
        }
    };
        
    save = async(req, res) => {
        try {
            this.authorsService.save(req.params.id, req.params.follow);
            return res.status(201).json({
                message: "Item successfully updated",
                success: true,
            });
        } catch(err) {
            console.log(err);
            return res.status(500).json({
                message: err.message,
                success: false,
            });
        }
    };

}

module.exports = AuthorsController;